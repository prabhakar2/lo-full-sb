trigger LeaveApplicationTgr on Leave_Application__c (before insert,before update) {
    Integer count=0;
    set<date>h=new set<date>();
    for(Holidays__c h1:[select Date__c from Holidays__c]){
        h.add(h1.Date__c);
    }
    for(Leave_Application__c le:trigger.new){
        for(date d=le.Start_From__c;d<=le.End_Date__c;d=d+1){
            DateTime day=DateTime.newInstance(d.year(), d.month(), d.day());
            if(day.format('EEEE')!='saturday' && day.format('EEEE')!='Sunday' && h.contains(d)==false)
            count++;
        }
        le.Number_Of_Leaves__c=count;
    }
}