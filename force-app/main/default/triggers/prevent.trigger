trigger prevent on Account (before insert) {
    set<String> acList=new set<String>();
    
    for(account ac : trigger.new){
        //System.debug('New Account Details :'+' '+ac);
        acList.add(ac.Name);
    }
    //System.debug('Set of account name'+' '+acList);
    List<account> la=[select name from account where name in :acList];
    //System.debug('Details of List'+' '+la);
    for(account a : trigger.new){
        //System.debug('Size of List of Account'+' '+la.size());
        if(la.size()>0)
            a.addError('Account already exist with same name');
    }
}