trigger ClosedOpportunityTrigger on Opportunity (before insert,before update) {
    List<Task> taskList = new List<Task>();
    for(Opportunity opp : Trigger.new) {
        if(Trigger.isInsert){
            if(Opp.StageName == 'Closed Won'){
                Task t1=new Task(Subject = 'Follow Up Test Task with insert', WhatId = opp.Id);
                taskList.add(t1);
            }
        }
        if(Trigger.isUpdate){
            if(Opp.StageName == 'Closed Won'){
                Task t1=new Task(Subject = 'Follow Up Test Task with update', WhatId = opp.Id);
                taskList.add(t1);
            }
        }       
    }
        insert taskList; 
    }