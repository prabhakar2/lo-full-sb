trigger noOfContact on Contact (after insert,before delete) {
      if(trigger.isInsert){
          set<id> NewconIds=new set<id>();
          for(contact newcon :trigger.new){
                NewconIds.add(newcon.AccountId);
            }
          map<id,account> acMap=new map<id,account>([select id,name,number_of_contact__c from account where id in :newconIds]);
          for(Contact cont: trigger.new){
              //system.debug(acMap.get(cont.AccountId).number_of_contact__c);
              acMap.get(cont.AccountId).number_of_contact__c++;
          }
          update acMap.values();
      }
    if(trigger.isdelete){
        set<id> OldConIds=new set<id>();
        for(Contact oldcon:trigger.old){
            OldconIds.add(oldcon.accountId);
        }
        map<id,account> acMap1=new map<id,account>([select id,name,number_of_contact__c from account where id in :oldconIds]);
            for(Contact cont: trigger.old){
               //system.debug(acMap1.get(cont.AccountId).number_of_contact__c);
               acMap1.get(cont.AccountId).number_of_contact__c--;
            }
         update acMap1.values();
    }
}