trigger DuplicateMonthYearCont on Contact (before insert,before update) {
    Set<Id> accIdSet = new Set<Id>();
    Map<String,Contact> contactMap = new Map<String,Contact>();
    for(Contact con : trigger.new){
        if(con.AccountId != NULL && con.Month__c != NULL && con.Year__c != NULL){
            accIdSet.add(con.AccountId);
            contactMap.put(con.AccountId+'@@'+con.Month__c+'@@'+con.Year__c, null);
        }
    }
    if(accIdSet.size() > 0 && contactMap.keySet().size() > 0){
        for(Contact con : [SELECT Id,Month__c,AccountId,Year__c FROM Contact 
                           WHERE AccountId != null 
                           AND AccountId IN :accIdSet
                           AND Month__c != null 
                           AND Year__c != null
                           AND Id NOT IN :trigger.new]){
                               String uniqueKey = con.AccountId+'@@'+con.Month__c+'@@'+con.Year__c;
                               if(contactMap.containsKey(uniqueKey)){
                                   contactMap.put(uniqueKey, con);
                               }
                           }
        System.debug('contactMap>>>'+contactMap);
        for(Contact con : trigger.new){
            if(con.AccountId != NULL && con.Month__c != NULL && con.Year__c != NULL){
                String uniqueKey = con.AccountId+'@@'+con.Month__c+'@@'+con.Year__c;
                if(contactMap.containsKey(uniqueKey)){
                    if(contactMap.get(uniqueKey) != null){
                        con.addError('Contact already exist for this month and year with same Account');
                    }
                }
            }
        }
    }
}