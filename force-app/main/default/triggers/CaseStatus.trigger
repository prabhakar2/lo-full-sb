trigger CaseStatus on Case (after update) {
    Set<Id> caseIdSet=new Set<Id>();
    for(Case c:trigger.new){
        if(c.Status=='Closed')
            caseIdSet.add(c.id);
    }
    
    if(caseIdSet!=null){
        
        Case Cs=[SELECT id,status,
                 (SELECT id,status from cases where status!='Closed'),
                 (SELECT id,status from Tasks where status!='Completed') 
                 from case 
                 where id IN :caseIdSet ];
        if(!Cs.cases.isEmpty() || !Cs.tasks.isEmpty()){
            Trigger.NewMap.get(cs.Id).addError('Please completed the all child cases & Tasks...');
        }
    }
    
}