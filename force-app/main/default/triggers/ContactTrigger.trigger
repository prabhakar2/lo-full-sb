trigger ContactTrigger on Contact (after update) {
    Map<Id,Id> contIdToAccIdMap = new Map<Id,Id>();
    for(Contact con : trigger.new){
        if(con.AccountId != NULL && con.AccountId != trigger.oldMap.get(con.Id).AccountId){
            contIdToAccIdMap.put(con.Id,con.AccountId);
        }
    }
    
    if(contIdToAccIdMap.keySet().size() > 0){
        List<Case> caseListToUpdate = new List<Case>();
        for(Case cs : [SELECT Id,ContactId,AccountId FROM Case WHERE ContactId IN :contIdToAccIdMap.keySet()]){
            cs.AccountId = contIdToAccIdMap.get(cs.ContactId);
            caseListToUpdate.add(cs);
        }
        if(caseListToUpdate.size() > 0)
            update caseListToUpdate;
    }
}