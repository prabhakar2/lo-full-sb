({
    doInit : function(component, event, helper) {
        var action=component.get("c.fetchCAD_Records");
        action.setParams({
            CADId : component.get("v.recordId")
        })
        action.setCallback(this, function(response){
            var state=response.getState();
            if(state=='SUCCESS'){
                component.set("v.TWRecords",response.getReturnValue());
                var tw = component.get("v.TWRecords");
             }
            else if (status === "ERROR") {
                       alert("Error: " + errorMessage);
            }
        });
        $A.enqueueAction(action);
    },
    handleOnLoad : function(component, event, helper){
        component.set("v.showSpinner",false);
    },
    handleSave : function(component, event, helper){
        
    },
    handleCancel : function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire();
    },
    handleOnSuccess : function(component, event, helper){
        
        var newRecord = event.getParams();
        var newRecordId = newRecord.response.id;  
        helper.showToast("Success","Success","Telephonic Witness record has been created successfully.");
    	helper.navigateToRecord(newRecordId);
    }
})