({
	showToast : function(type,title,msg) {
		var toastEvent = $A.get("e.force:showToast").setParams({"type": type,"title": title,"message": msg}).fire();
	},
    
    navigateToRecord : function(newRecordId){
        $A.get("e.force:navigateToSObject").setParams({"recordId": newRecordId,"slideDevName": "detail"}).fire();
        
    }
})