/*
    * @ Class Name  	: 	Case_DataArchiveBatch
    * @ Description 	: 	Batch Class for Archive the Email Messages & Attachments of Case Object.
    * @ Created By  	: 	Prabhakar Joshi
    * @ Created Date	: 	14-July-2020
    * @ Last Modified   :   16-Nov-2020
*/

global class Case_DataArchiveBatch implements Database.Batchable<sobject>, Database.Stateful{
    /* To Create a instance of the Handler Class. */
    Case_DataArchiveHandler handler = new Case_DataArchiveHandler();
    /* To hold the count of total deleted records. */
    Integer deleteCount;
    /* To hold the count of total failed records. */
    Integer failCount;
    /* Map to hold the record Id and error in record deletion which has been failed.  */
    Map<Id,String> recordIdToErrorMap;
    /* To hold the limit value which will specify that how much case records are processing. */
    Integer limitSelected;

    /* @ Consturctor */
    public Case_DataArchiveBatch(String selectedLimit){
        this.deleteCount = 0;
        this.failCount = 0;
        this.recordIdToErrorMap = new Map<Id,String>();
        this.limitSelected = Integer.valueOf(selectedLimit);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        Date eighteenMonthBeforeDate = Case_DataArchiveHandler.eighteenMonthBeforeDate;
        Set<Id> recordTypeIdSet = handler.getCaseRecordTypeIdSet();

        String query = 'SELECT Id,RecordType.Name,Status,LastmodifiedDate,Contact.Inactive_Contact__c,Data_Archived_Date__c, ';
        query += '(SELECT Id  FROM EmailMessages WHERE LastModifiedDate < :eighteenMonthBeforeDate),';
        query += '(SELECT Id FROM Attachments WHERE LastModifiedDate < :eighteenMonthBeforeDate) FROM Case WHERE Status = \'Closed\' ';
        query += 'AND RecordTypeId IN : recordTypeIdSet AND Contact.Inactive_Contact__c = false AND ((Data_Archived_Date__c != NULL ';
        query += 'AND Data_Archived_Date__c < :eighteenMonthBeforeDate) OR(Data_Archived_Date__c = NULL AND LastmodifiedDate < :eighteenMonthBeforeDate)) ';
        query += 'ORDER BY LastmodifiedDate LIMIT :limitSelected';

        return DataBase.getQueryLocator(query);
      
    }
    
    
    global void execute(Database.BatchableContext bc, List<Case> recordList){
        List<Sobject> sobjList = new List<Sobject>();
        Map<Id,Case> caseMap = new Map<Id,Case>();
        caseMap.putAll(recordList);

        for(Case cs : recordList){
            if(!cs.Attachments.isEmpty()){
                sobjList.addAll(cs.Attachments);
            }
            if(!cs.EmailMessages.isEmpty()){
                sobjList.addAll(cs.EmailMessages);
            }
        }
        if(!sobjList.isEmpty()){
            List<Database.DeleteResult> result = Database.delete(sobjList, false);
            for(Database.DeleteResult dr : result){
                if(dr.isSuccess()){
                    this.deleteCount ++;
                }else{
                    this.failCount ++;
                    for(Database.Error err : dr.getErrors()){
                        recordIdToErrorMap.put(dr.getId(), err.getMessage());
                    }
                }
            }
        }
        for(Case cs : caseMap.values()){
            cs.Data_Archived_Date__c = System.today();
        }
        update caseMap.values();
    }
    
    global void finish(Database.BatchableContext bc){
        if(this.deleteCount > 0 || this.failCount > 0 || !recordIdToErrorMap.isEmpty()){
            /* Calling the handler method to send the notification after archiving the data. */
            handler.sendNotificationAfterAttachmentDelete(this.deleteCount,this.failCount,recordIdToErrorMap, 'Case Data Archive Notification');
        }
    }
}