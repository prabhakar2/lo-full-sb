@isTest
public class DailyLeadProcessorTest {
	@isTest
    static void test1(){
        String CRON_EXP = '0 0 1 * * ?';

        List<Lead> ldList = new List<Lead>();
        for(Integer i=1;i<=200;i++){
            Lead ld = new Lead();
            ld.LastName = 'test '+i;
            ld.Company = 'TMC';
            ldList.add(ld);
        }
        
        insert ldList;
        
        Test.startTest();
        String jobId = System.schedule('DailyLeadProcessor',CRON_EXP, new DailyLeadProcessor());
        test.stopTest();
    }
}