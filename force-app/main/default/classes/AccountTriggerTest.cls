@isTest
private class AccountTriggerTest {
    @isTest static void mytest(){
        List<account> aclist=new List<account>();
        for(Integer i=1;i<=200;i++){
            account ac=new account(name='Test Account',billingState='CA');
            aclist.add(ac);
        }
        test.startTest();
            insert aclist;
        test.stopTest();
        list<account>lstAc=[select shippingState from account];
        for(account a:lstAc)
            System.assertEquals('CA',a.shippingState,'error');
    }
}