public class InsertAccountAura {
    @auraEnabled
    public static String saveAccount(Account acc){
        if(acc!=null){
            try{
                insert acc;
                return 'Success';
            }catch(Exception e){
                return 'Error :'+e.getMessage();
            }
        }else
            return null;
    }
}