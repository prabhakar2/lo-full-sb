public abstract class Employee
{
	public String empId,name,add,contact,designation;
   
    public Employee(String id,String name, String ad, String cont,String des)
    {
       this.empId=id;
        this.name=name;
        this.add=ad;
        this.contact=cont;
        this.designation=des;
    }
    
    public abstract double calculateSalary();
    
}