public class Permanent extends Employee
{
      double basicSalary,hra,tax;
   
    public Permanent(String id,String name, String ad, String cont,String des, double h, double t, double b)
    {
        super(id,name,ad,cont,des);
 		this.basicSalary=b;     
        this.hra=h;
        this.tax=t;
        
    }
    public override double calculateSalary()
    {
        double newSal=basicSalary+hra+tax;
        return newSal;
        
        
    }
}