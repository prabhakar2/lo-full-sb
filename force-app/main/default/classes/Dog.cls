public class Dog {
                public String name;
                private String status;
                private Dog playmate; 
                
    public void sit()
    			{
                status = 'Sitting';
                }
                public void play(Dog newPlaymate)
                			{
                                status = 'Playing';
                                if(playmate != newPlaymate) 
                                {
                                      playmate = newPlaymate;
                                      newPlaymate.play(this);
                                }
             			     }
                public void wagTail(){
                status = 'Wagging Tail';
                }
               
    			public String getStatus(){
                return status;
                }
               
    			public String getPlaymateName()
                {
                                if(status=='Playing')
                                {
                                                return playmate.name;
                                } else 
                                {
                                                return null;
                                }
                }
}