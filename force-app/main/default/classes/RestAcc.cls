@RestResource(urlMapping='/account/*')

global class RestAcc {
   @HttpPost
   global static void createAccount( String Name, String AccountNumber, String Description) {
        account ac = new account(
           
            Name=name,
            AccountNumber=AccountNumber,
            Description=Description
           );
        insert ac;
    }
    
    @HttpGet
    global static List<Account> getAllAccount(){
        RestRequest request=restContext.request;
        List<Account> listAc=[select id,name from Account];
        return listAc;
    }
    
    @HttpPatch
    global Static Account updateAccount(String name, String AccountNumber ,String Description){
        Account updateAc=[Select id,name,AccountNumber,Description from Account where name=:name];
        updateAc.name=name;
        updateAc.AccountNumber=AccountNumber;
        updateAc.Description=Description;
        update updateAc;
        return updateAc;
    }

}