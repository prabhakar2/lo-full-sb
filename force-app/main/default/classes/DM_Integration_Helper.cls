/***********************************************************************************
 * Class Name 	: DM_Integration_Helper
 * Description	: Helper class for DM_Integration
 * Created By 	: Prabhakar Joshi
 * Created Date	: 22-11-2019
 * *******************************************************************************/
public class DM_Integration_Helper {
    
    /************************************* Method to process the JSON Data **************************************/
    public static void processJSONData(String jsonWrapStr){
        
        /***************************** Deserialize into the JSONWrapper Type ***************************************/
        JSONWrapper jsonWrap = (JSONWrapper)JSON.deserialize(jsonWrapStr, JSONWrapper.class);
        
        if(jsonWrap.status.code == 200 && jsonWrap.status.message == 'Success'){
            
            /********************* To get the Map of Custom Setting for dynamic field Mapping ****************/
            Map<String,DM_API_Field_Mapping__c> fieldMapping = DM_API_Field_Mapping__c.getAll();
            
            /******************************** Final List to insert the PAM Records **********************************/
            List<Process_Adherence_Monitoring__c> pamListToInsert = new List<Process_Adherence_Monitoring__c>();
            
            /******************************* Map of Firm Id and Account Id *********************************/
            Map<String,Id> firmIdToAccIdMap = new Map<String,Id>();
            
            /***************************** To get the record Type Ids ************************************/
            Id DM_PSNM_RecordTypeId = Schema.SObjectType.Process_Adherence_Monitoring__c.getRecordTypeInfosByName().get('DM PSNM').getRecordTypeId();
            Id DM_RSNM_RecordTypeId = Schema.SObjectType.Process_Adherence_Monitoring__c.getRecordTypeInfosByName().get('DM RSNM').getRecordTypeId();
            
            /*************************** Map of unique key and PAM Id **********************************/
            Map<String,Id> uniqueIdToPAMMap = new Map<String,Id>();
            
            /*************************** Map for JSON data which is coming from response **************************/
            List<Map<String,String>> DataMap = (List<Map<String, String>>)JSON.deserialize(JSON.serialize(jsonWrap.data),List<Map<String, String>>.class);
            
            /**************************** Iterate the JSON Data *******************************/
            for(Map<String,String> dw : DataMap){
                Id recordTypeId;
                if(dw.get('reactiveFlag') != NULL || dw.get('proactiveFlag') != NULL){
                    if(String.isNotBlank(dw.get('suitTheoryDate'))){
                        if(dw.get('reactiveFlag') == 'No' || dw.get('reactiveFlag') == 'Unknown'){
                            recordTypeId = DM_RSNM_RecordTypeId;
                        }else if(dw.get('proactiveFlag') != NULL){
                            recordTypeId = DM_PSNM_RecordTypeId;
                        }
                        String uniqueKey = recordTypeId +'@@'+(String.valueOf(dw.get('suitTheoryDate'))).split('T')[0];
                        uniqueIdToPAMMap.put(uniqueKey,NULL);
                    }
                }
                if(String.isNotBlank(dw.get('firmID'))){
                    firmIdToAccIdMap.put(dw.get('firmID'),NULL);
                }
            }
            
            if(firmIdToAccIdMap.keySet().size() > 0){
                for(Account acc : [SELECT Id,Firm_Id__c FROM Account 
                                   WHERE Firm_Id__c IN :firmIdToAccIdMap.keySet() 
                                   AND Firm_Id__c != NULL]){
                                       if(firmIdToAccIdMap.containskey(acc.Firm_ID__c)){
                                           firmIdToAccIdMap.put(acc.Firm_Id__c,acc.Id);
                                       }
                                   }
            }
            
            if(uniqueIdToPAMMap.keySet().size() > 0){
                for(Process_Adherence_Monitoring__c pam : [SELECT Id,RecordTypeId,STNM_Suit_Theory_Code_Date__c 
                                                           FROM Process_Adherence_Monitoring__c
                                                           WHERE (RecordTypeId =:DM_PSNM_RecordTypeId 
                                                                  OR RecordTypeId =:DM_RSNM_RecordTypeId)
                                                           AND STNM_Suit_Theory_Code_Date__c != NULL]){
                                                               
                                                               String uniqueKey = pam.RecordTypeId +'@@'+(String.valueOf(pam.STNM_Suit_Theory_Code_Date__c)).split(' ')[0];
                                                               if(uniqueIdToPAMMap.containsKey(uniqueKey)){
                                                                   uniqueIdToPAMMap.put(uniqueKey,pam.Id);
                                                               }
                                                           }
            }
            System.debug('>>>>>>uniqueIdToPAMMap>>>>>>'+uniqueIdToPAMMap);
            for(Map<String,String> dw : DataMap){
                if(dw.get('reactiveFlag') != NULL || dw.get('proactiveFlag') != NULL){
                    Id recordTypeId;
                    
                    if(dw.get('reactiveFlag') == 'No' || dw.get('reactiveFlag') == 'Unknown'){
                        recordTypeId = DM_RSNM_RecordTypeId;
                    }else if(dw.get('proactiveFlag') != NULL){
                        recordTypeId = DM_PSNM_RecordTypeId;
                    }
                    String key = recordTypeId +'@@'+(String.valueOf(dw.get('suitTheoryDate'))).split('T')[0];
                    
                    if(uniqueIdToPAMMap.containsKey(key) && uniqueIdToPAMMap.get(key) == NULL){
                        
                        Process_Adherence_Monitoring__c pam = new Process_Adherence_Monitoring__c();
                        pam.RecordTypeId = key.split('@@')[0];
                        pam.Organization_Name__c = firmIdToAccIdMap.get(dw.get('firmID'));
                        //pam.STNM_Suit_Theory_Code_Date__c = Date.valueOf(key.split('@@')[1]);
                        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get('Process_Adherence_Monitoring__c').getDescribe().fields.getMap();
                        
                        for(String sfField : fieldMapping.keySet()){
                            String fielddataType = String.valueOf(fieldMap.get(sfField).getDescribe().getType());
                            if(fieldMapping.get(sfField).Active__c){
                                if(fielddataType == 'Date'){
                                    pam.put(sfField,Date.valueOf(dw.get(fieldMapping.get(sfField).API_Parameter__c)));
                                }
                                else if(fielddataType == 'DOUBLE' || fielddataType == 'CURRENCY'){
                                    pam.put(sfField,Decimal.valueOf(dw.get(fieldMapping.get(sfField).API_Parameter__c)));
                                }
                                else if(fielddataType == 'BOOLEAN'){
                                    pam.put(sfField,Boolean.valueOf(dw.get(fieldMapping.get(sfField).API_Parameter__c)));
                                }
                                else{
                                    pam.put(sfField,dw.get(fieldMapping.get(sfField).API_Parameter__c));
                                }
                            }
                        }
                        System.debug('>>>>>pam>>>>>>'+pam);
                        
                        pamListToInsert.add(pam);
                        
                    }
                }
            }
            
            System.debug('>>>>>pamListToInsert>>>>>>'+pamListToInsert.size());
            if(pamListToInsert.size() > 0){
                String accountNumber;
                String errorFields ;
                String errorDescription;
                Database.SaveResult[] result = Database.insert(pamListToInsert, false); 
                for(Integer i=0;i<result.size();i++){
                    if(!result[i].isSuccess()){
                        accountNumber = pamListToInsert[i].Account_Number__c;
                        for(Database.Error err : result[i].getErrors()){
                            errorFields = String.join(err.getFields(),',');
                            errorDescription = err.getMessage();
                            sendFailedRecordDescription(accountNumber,errorFields,errorDescription);
                        }
                    }
                }
            }
        }
    }
    
    private static void sendFailedRecordDescription(String accNumber,String errorFields,String description){
        List<OrgWideEmailAddress> orgWideEmail = [SELECT Id, Address, DisplayName, IsAllowAllProfiles 
                                                  FROM OrgWideEmailAddress 
                                                  WHERE Address = 'lcsalesforceadmin@mcmcg.com' LIMIT 1];
        if(orgWideEmail.size() > 0){
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setSubject('DM PAM Failed Records Detail');
            String htmlBody = '<html><body>';
            htmlBody += 'An error has occurred during the API Call for PSNM/RSNM/COT Affidavit Process that has prevented a record from being created.<br/><br/>';
            htmlBody += '<table border="1" cellspacing="0" cellpadding="5"><thead>';
            htmlBody += '<tr><th>Account Number</th><th>Error Field</th><th>Description</th></tr>';
            htmlBody += '<tr><td>'+accNumber+'</td><td>'+errorFields+'</td><td>'+description+'</td></tr>';
            htmlBody += '</thead></table><br/><br/>';
            htmlBody += 'Please see details of exception below:<br/>';
            htmlBody += '<ul><li>Account Number</li><li>Firm Id</li><li>Original Creditor</li><li>State</li>';
            htmlBody += '<li>Placement Date</li><li>Suit Theory Code Date</li><li>Missing Media Type</li><li>PSNM Flag</li>';
            htmlBody += '<li>RSNM Flag</li><li>COT Flag</li></ul>Thanks,<br/><br/>System Administrator';
            htmlBody += '</body></html>';
            email.setHtmlBody(htmlBody);
            email.setOrgWideEmailAddressId(orgWideEmail[0].Id);
            email.toaddresses = new List<String>{'prabhakar.joshi@mcmcg.com'}; //lcsalesforceadmin@mcmcg.com // LO-Operational@mcmcg.com
                Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
        }
    }
    
    public class JSONWrapper{
        public StatusWrapper status;
        public List<DataWrapper> data;
        public String custom;
        public String timestamp;
    }
    
    public class StatusWrapper{
        public Integer code;
        public String message;
        public String uniqueErrorId;
        public String messageCode;
    }
    
    public class DataWrapper{
        public String consumerAccountId;
        public String consumerAccountIdentifierAgencyId;
        public String cotAffidavitRemediation;
        public String cotAffidavitSent;
        public String placementDate;
        public String originalAccountNumber;
        public String reactiveFlag;
        public String proactiveFlag;
        public String missingMedia;
        public String oaldVerificationStatus;
        public String mediaLegalElgibility;
        public String reactiveMissingDocTypes;
        public String oaldModifiedDate;
        public String oaldModifiedBy;
        public String proactiveMissingDocTypes;
        public String lastUpdateDate;
        public String proactiveRemediationStatus;
        public String reactiveRemediationStatus;
        public String originalCreditorName;
        public String sellerName;
        public String firmID;
        public String state;
        public String suitTheoryDate;
    }
}