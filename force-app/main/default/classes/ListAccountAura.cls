public class ListAccountAura {
    @AuraEnabled
    public static List<Account> getAccounts(){
        List<Account> listAcc = [SELECT Id,Name FROM Account];
        System.debug('Account----->'+listAcc);
        if(listAcc.size()>0)
            return listAcc;
        else
            return null;
    }
    
}