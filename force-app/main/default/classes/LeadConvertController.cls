public class LeadConvertController {
    public List<Lead> leadList{get;set;}
    public String lid{get;set;}
    public List<Task> taskList{get;set;}
    public String remdate{get;set;}
    public String tym{get;set;}
    
    
    public LeadConvertController(){
        lid=ApexPages.currentPage().getparameters().get('id');
        
        leadList=[select ownerid,PartnerAccountid,firstname,lastname,Status,Company,
                  (select reminderDateTime,description from tasks) 
                  from lead where id=:lid];
        
        //tasklist=leadList[0].tasks;
        //DateTime dT=taskList[0].reminderDateTime;
        //date myDate=date.newInstance(dT.year(), dt.month(), dt.day());
        //remdate=String.valueOf(myDate);
        //system.debug('ok--'+myDate);
    }
    
    public List<SelectOption> getAccountNames() {
        List<SelectOption> accOptions= new List<SelectOption>();
        accOptions.add( new SelectOption('','--Select--'));
        for( Account acc : [select Id,name from Account ] ) {
            accOptions.add( new SelectOption(acc.Id,acc.name));
        }
        return accOptions;
   }
    
    public list<SelectOption> getTyms() {     
        list<SelectOption> options = new list<SelectOption>();
        
        options.add(new SelectOption('8:00AM','8:00AM'));
        options.add(new SelectOption('9:00AM','9:00AM'));                     
        options.add(new SelectOption('10:00AM','10:00AM'));
        options.add(new SelectOption('11:00AM','11:00AM'));
        options.add(new SelectOption('12:00PM','12:00PM'));
        options.add(new SelectOption('1:00PM','1:00PM'));
        options.add(new SelectOption('2:00PM','2:00PM'));
        options.add(new SelectOption('3:00PM','3:00PM'));
        options.add(new SelectOption('4:00PM','4:00PM'));
        options.add(new SelectOption('5:00PM','5:00PM'));
        options.add(new SelectOption('6:00PM','6:00PM'));
        options.add(new SelectOption('7:00PM','7:00PM'));
        options.add(new SelectOption('8:00PM','8:00PM'));
        options.add(new SelectOption('9:00PM','9:00PM'));
        options.add(new SelectOption('10:00PM','10:00PM'));
        options.add(new SelectOption('11:00PM','11:00PM'));
        options.add(new SelectOption('12:00AM','12:00AM'));
        options.add(new SelectOption('1:00AM','1:00AM'));
        options.add(new SelectOption('2:00AM','2:00AM'));
        options.add(new SelectOption('3:00AM','3:00AM'));
        options.add(new SelectOption('4:00AM','4:00AM'));
        options.add(new SelectOption('5:00AM','5:00AM'));
        options.add(new SelectOption('6:00AM','6:00AM'));
        options.add(new SelectOption('7:00AM','7:00AM'));
        System.debug('option'+options);
        return options;           
    } 
    
}