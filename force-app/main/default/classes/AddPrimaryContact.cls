public class AddPrimaryContact implements Queueable{
    private Contact cont;
    private String state;
    public AddPrimaryContact(Contact cont,String state){
        this.cont = cont;
        this.state = state;
    }
    public void execute(QueueableContext qc){
        List<Contact> conList = new List<Contact>();
        for(Account acc : [SELECT Id,Name,
                           (SELECT Id,FirstName,LastName FROM Contacts)
                           FROM Account
                           WHERE BillingState =:state 
                           LIMIT 200]){
                               Contact con = cont.clone(false,false,false,false);
                               con.AccountId = acc.Id;
                               conList.add(con);
                           }
        if(conList.size() > 0){
            insert conList;
        }
    }
}