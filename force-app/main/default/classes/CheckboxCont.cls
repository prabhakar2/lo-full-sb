public class CheckboxCont {
    public contact__c editcon{get;set;}
    public contact__c conEdit{get;set;}
    Contact__c cont=new Contact__c();   
    public List<Contact__c> listCoNtact{ get;set;}
    public List<Wrap> ContactList{get;set;}
    List<Contact__c> selectedContact = new List<Contact__c>();
    
    
    //-------------------for delete-----------
    public List<wrap> getContacts(){
        if(ContactList==null){
            ContactList=new List<wrap>();
            for(Contact__c con : [select id,Last_name__c,First_Name__c,Account__c,phone__C from contact__c])
                ContactList.add(new wrap(con));
        }
        return ContactList;     
    }
    
    
    ////-------------------constructor--------------------////
    public CheckboxCont() {
      listContact=new List<Contact__c>();
      listContact.add(cont);
    }
    public void addContact() {
       Contact__c c1=new Contact__c();
        listContact.add(c1);
    }
      public void saveContact() {
        insert listContact;
        listContact.clear();
        listContact.add(new Contact__c());
    } 
    
    public void deleteChecked() {
        
        //-------ADDING SELECTED CONTACTS TO A NEW CONTACT LIST
        for(wrap w1:contactList){
            if(w1.check==TRUE){
                selectedContact.add(w1.con);
            }
        }
        delete selectedContact;
        selectedContact.clear();
        contactList=Null;
        //return null;
    }
    
     
   public PageReference edit(){
       conEdit=new contact__c();
       update conEdit; 
       return null;
   }
   
   public PageReference SaveInline(){
       return null;
   }
    
    
    //------------------wrapper class-----------------
    public class Wrap {
    public contact__c con{get;set;}
    public boolean check{get;set;}
    public wrap(Contact__c c){
        con=c;
        check=false;
        }
   }
    
}