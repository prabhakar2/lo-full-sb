public class WrapperExampleCntr {
    public List<wrapper> wrapList{get;set;}
    public List<Account> selectedRecords{get;set;}
    public String accIdForDel{get;set;}
    
    public WrapperExampleCntr(){
        wrapList=new List<wrapper>();
        selectedRecords=new List<Account>();
        for(Account ac : [SELECT id,name FROM Account ORDER BY CreatedDate desc limit 20]){
            wrapList.add(new wrapper(ac));
            System.debug('>>>>wrapList>>>>'+wrapList);
        }
    }
    
    public void addToDelList(){
        System.debug('accIdForDel>>>>>>'+accIdForDel);
        if(accIdForDel!=NULL){
            Account acc=[SELECT id,name FROM Account where id=:accIdForDel];
            System.debug('>>>>>>>>acc>>>>>>>>'+acc);
            selectedRecords.add(acc);
        }
        
    }
    public void delRecords(){
        if(selectedRecords.size()>0){
            delete selectedRecords;
        }else
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,'Please select any record to Delete.'));
    }
    
    public class wrapper{
        public Account acc{get;set;}
        public boolean isSelect{get;set;}
        public wrapper(Account a){
            acc=a;
            isSelect=false;
        }
    }
    
    
}